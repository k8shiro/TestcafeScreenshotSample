//const ENDPOINT_URL = 'http://' + process.env.EXTERNAL_HOSTNAME + ':8888';
const ENDPOINT_URL = 'https://www.google.com/'

import { Selector } from 'testcafe';

fixture('Screenshot').page(ENDPOINT_URL)

test('screenshot_sample_1', async t => {
    let searchTextArea = Selector('.gLFyf')
    let searchButton   = Selector('.gNO89b')
    let imageTextArea  = Selector('.JSAgYe')
    let imageButton    = Selector('#hdtb-msb-vis > div:nth-child(2)')
    await t
	.typeText(searchTextArea, 'チベットスナギツネ')
	.click(searchButton)
        .click(imageButton)
	.wait(2000)
	.expect(imageTextArea.value).contains('チベットスナギツネ')
	.takeScreenshot();
});

test('screenshot_sample_2', async t => {
    let searchTextArea = Selector('.gLFyf')
    let searchButton   = Selector('.gNO89b')
    let imageTextArea  = Selector('.JSAgYe')
    let imageButton    = Selector('#hdtb-msb-vis > div:nth-child(2)')
    await t
        .typeText(searchTextArea, "ハシビロコウ")
        .click(searchButton)
        .click(imageButton)
        .wait(2000)
	.expect(imageTextArea.value).contains('チベットスナギツネ')
});
