# TestcafeScreenshotSample

testcafeをGitlab CIで実行し、スクリーンショットをとってダウンロードするSample

# 使い方

```
export EXTERNAL_HOSTNAME=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)

docker run \
    --add-host=${EXTERNAL_HOSTNAME}:127.0.0.1 \
    -p 1337:1337 -p 1338:1338 \
    -v $(pwd)/testcafe/tests:/tests -e EXTERNAL_HOSTNAME=${EXTERNAL_HOSTNAME} \
    -v $(pwd)/testcafe/screenshots:/screenshots \
    -v $(pwd)/testcafe/.testcaferc.json:/.testcaferc.json \
    -it testcafe/testcafe  --hostname ${EXTERNAL_HOSTNAME} remote /tests/test.js 

docker run \
    --add-host=${EXTERNAL_HOSTNAME}:127.0.0.1 \
    -p 1337:1337 -p 1338:1338 \
    -v $(pwd)/testcafe/tests:/tests -e EXTERNAL_HOSTNAME=${EXTERNAL_HOSTNAME} \
    -v $(pwd)/testcafe/screenshots:/screenshots \
    -v $(pwd)/testcafe/.testcaferc.json:/.testcaferc.json \
    -it testcafe/testcafe  --hostname ${EXTERNAL_HOSTNAME} chromium /tests/test.js
```
